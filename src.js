//https://www.codingame.com/training/expert/nintendo-sponsored-contest
//https://www.codingame.com/servlet/fileservlet?id=1915207099028

//most frequent 541a4231 5d324646 27219a26 12497b0e 724eddcb 0e131617 9521bedf 55544dc7

var size = 32;
var shrunkSize = Math.floor(size / 16);
var format = 16; //hex
var forceUseAll = false;
//var format = 10; //base 10
//var target = parseInt("11101", 2);
var target = 29615;
//var target = 12345;
//var target = 1937995713; //1110011100000110111011111000001
//var target = parseInt("110111011111000001", 2);

var candidates = [];
var checked = [];


var totalIterations = 0; //for debugging

function paddy(n, p=8, f=format) {
    n = n.toString(f);
    var pad_char = '0';
    var pad = new Array(1 + p).join(pad_char);
    return (pad + n).slice(-pad.length);
}

function outputResult(ret) {
	var str = "";
	ret.forEach(function(num, index) { str += num + " "; });
	console.log(str);
}

function Forward(a, start=0) {
	//when passing the start, we need to make sure the remaining numbers are zero
	var isValidateOnly = (start != 0);
	var b = [];
	for (var i = 0; i < shrunkSize; i++) {   // Write size / 16 zeros to b
		b[i] = 0;
	}
	for (var i = 0; i < size; i++) {
		for (var j = 0; j < size; j++) {
			var zeroOrOne = Math.floor((i + j) / 32); //complicated way to say 0?
			var bPos = (i + j) % 32; //bit in b[0] we want to change
			var first = a[Math.floor(i / 32)]; //complicated way to say a[0]?
			var second = a[Math.floor((j / 32) + (size / 32))]; //complicated way to say a[1]?
			var firstPos = i % 32;
			var secondPos = j % 32;
			var firstBit = first >> firstPos;
			var secondBit = second >> secondPos;
			var isOdd = (firstBit & secondBit & 1) == 1;
			if(isOdd) {
				if(isValidateOnly && bPos >= start) {
					return -1;
				}
				//console.log("x"+paddy(firstPos, 2, 10) + " & y"+paddy(secondPos, 2, 10)+" =1", bPos);
				b[zeroOrOne] ^= 1 << bPos;   // Magic centaurian operation
				//console.log(paddy(b[zeroOrOne].toString(2), 32));
				//console.log("b["+bIndex+"]^=pow(2,"+bPos+")", i, j);
				
			}
		}
	}
	if(isValidateOnly) return 0;
	var ret = [];
	for(var i = 0; i < shrunkSize; i++) {
		ret[i] = paddy(b[i]);
	}
	return ret;

}
function checkSkip(x, y) {
	//return false;
	var xRev = JSON.parse(JSON.stringify(x)).reverse();
	var xChecksum = parseInt(xRev.join(""), 2)
	var yRev = JSON.parse(JSON.stringify(y)).reverse();
	var yChecksum = parseInt(yRev.join(""), 2);
	var isSkip = (checked.indexOf(yChecksum) >= 0);
	if(!isSkip) {
		checked.push(xChecksum);
	}
	return isSkip;
}

function Backward_inner(target, targetBinary, pairs, x, y, bPos, size) {
	totalIterations++;
	if(checkSkip(x, y)) return; //we can add inverses of results at the end, don't check here
	var possible = [[0,0],[0,1],[1,0],[1,1]];
	var curPairs = pairs[bPos];
	var val = targetBinary[bPos];
	if(bPos < size) {
		//we can assume there are 2 unanswered values in the next row
		//find out the possible valid substitutions and try all of them
		var valid = [];
		var unknownX = -1;
		var unknownY = -1;
		possible.forEach(function(vals) {
			var test = 0;
			curPairs.forEach(function(curPair, index) {
				var xVal = x[curPair[0]];
				var yVal = y[curPair[1]];
				if(xVal == undefined || xVal == null) {
					unknownX = curPair[0];
					xVal = vals[0];
				}
				if(yVal == undefined || yVal == null) {
					unknownY = curPair[1];
					yVal = vals[1];
				}
				test ^= (xVal & yVal);
			});
			if(test == val) {
				valid.push(vals);
			}
		});
		//console.log("valid substitions for unknown entries in row 1:", valid);
		valid.forEach(function(validPair,index) {
			xCp = JSON.parse(JSON.stringify(x));
			yCp = JSON.parse(JSON.stringify(y));
			xCp[unknownX] = validPair[0];
			yCp[unknownY] = validPair[1];
			var okToPush = true;
			if(bPos == size - 1) {
				//not really optimizing, just moving check here
				//remove cases where there are exta 1s added in bits position size to 32
				var answer = Forward([parseInt(xCp.join(""), 2), parseInt(yCp.join(""), 2)], size);
				okToPush = (answer != -1);
			}
			if(forceUseAll || okToPush) Backward_inner(target, targetBinary, pairs, xCp, yCp, bPos+1, size);
		});
	} else {
		candidates.push([parseInt(x.join(""), 2), parseInt(y.join(""), 2)]);
	}

}

function Backward(target) {
	var targetSize = target.toString(2).length;
	//targetSize = size;
	var targetBinary = paddy(target, targetSize, 2).split("").reverse().join("");
	var debugPairs = [[],[]];
	var pairs = [[],[]];
	var modSize = targetSize;
	for (var i = 0; i < targetSize; i++) {
		//for (var j = i; j < targetSize; j++) {
		for (var j = 0; j < targetSize; j++) {
			var zeroOrOne = Math.floor((i + j) / modSize); //complicated way to say 0?
			var bPos = (i + j) % modSize;
			var val = (zeroOrOne == "0") ? targetBinary[bPos] : "0";
			if(pairs[zeroOrOne][bPos] == undefined) pairs[zeroOrOne][bPos] = [];
			if(debugPairs[zeroOrOne][bPos] == undefined) debugPairs[zeroOrOne][bPos] = "@" + bPos + " " + val + " = ";
			var firstPos = i % modSize;
			var secondPos = j % modSize;
			//need to inverse back into position
			var firstPosInv = targetSize - firstPos - 1;
			var secondPosInv = targetSize - secondPos - 1;
			pairs[zeroOrOne][bPos].push([firstPosInv, secondPosInv]);
			debugPairs[zeroOrOne][bPos] += "(x" + paddy(firstPos, 2, 10) + " & y" + paddy(secondPos, 2, 10) + ")";
			debugPairs[zeroOrOne][bPos] += " ^ ";
		}
	}
	debugPairs[0].forEach(function(line) { console.log(line.substr(0, line.length - 3)); });
	console.log("-------");
	debugPairs[1].forEach(function(line) { console.log(line.substr(0, line.length - 3)); });
	
	//deduce all that we can about the pairs
	Backward_inner(target, targetBinary, pairs[0], [], [], 0, targetSize);
	return candidates;
}

/*
var as = [
	[1, 29615],
	[131, 229],
	[29615, 1],
	[229, 131]
];
outputResult(Forward(as[1]));
//outputResult(Forward([2,3116481504]));
*/

var candidates = Backward(target);
console.log("Target:", paddy(target, 32, 2));
console.log("Iterations:", totalIterations);
//weed out bad candidates
var discarded = 0;
var as = [];
candidates.forEach(function(a,index) {
	var b = Forward(a);
	var answer = parseInt(b[0], format);
	//if(answer != target) throw "Backward didn't work: " + answer + "!=" + target;
	if(answer == target) {
		as.push(a);
		/*console.log("OK!");
		var targetSize = target.toString(2).length;
		console.log(paddy(a[0], targetSize, 2).split("").reverse().join(""));
		console.log(paddy(a[1], targetSize, 2).split("").reverse().join(""));
		*/
	} else  {
		discarded++;
		/*
		if(discarded == 1) {
		console.log("---Wrong answer---");
		console.log(paddy(answer, 32, 2));
		var targetSize = target.toString(2).length;
		console.log(paddy(a[0], targetSize, 2).split("").reverse().join(""));
		console.log(paddy(a[1], targetSize, 2).split("").reverse().join(""));
		console.log("------");
		}
		*/
	}
});
console.log("Discarded: ", discarded);
//show output
var str="\n";
as.forEach(function(a) {
	str+="		cout << \"" + paddy(a[0]) + " " + paddy(a[1]) + "\" << endl;\n";
});
//go backwards to get remaining results
as.reverse().forEach(function(a) {
	str+="		cout << \"" + paddy(a[1]) + " " + paddy(a[0]) + "\" << endl;\n";
});
console.log(str);


