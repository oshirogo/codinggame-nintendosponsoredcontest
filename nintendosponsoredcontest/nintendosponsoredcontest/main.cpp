#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <chrono>
#include <unordered_set>
#include <sstream>
#include <thread>
#include <bitset>
#include <climits>

#define MAX_THREADS 64
#define SPAWN_INDEX 6

using namespace std;

typedef signed short int u8;
typedef unsigned int u32;

int size;
int shrunkSize;
u32 target0;
u32 target1;

//atomic_int num_threads(0);
int num_threads;
vector<vector<pair<u32, u32>>> pairs;
vector<vector<pair<u32, u32>>> candidates;
vector<thread> threads;
u8 msb;
u32 iterations;


vector<u32> Forward(vector<unsigned int> a, int validateStart) {
    auto isValidate = (validateStart >= 0);
    vector<u32> b(shrunkSize);
    for (auto i = 0; i < shrunkSize; i++) {   // Write size / 16 zeros to b
        b[i] = 0;
    }
    for (auto i = 0; i < size; i++) {
        for (auto j = 0; j < size; j++) {
            auto bPos = (i + j) % 32;
            if(bPos < validateStart) continue;
            auto bIndex = (i + j) / 32;
            auto aIIndex = i / 32;
            auto aJIndex = (j / 32) + (size / 32);
            auto iMod = i % 32;
            auto jMod = j % 32;
            auto rightShiftIMod = a[aIIndex] >> iMod;
            auto rightShiftJMod = a[aJIndex] >> jMod;
            auto isOdd = (rightShiftIMod & rightShiftJMod & 1) == 1;
            if(isOdd) {
                if(isValidate && bPos >= validateStart) {
                    return {};
                }
                //if(isValidate && bIndex > 0) {
                //    return {};
                //}
                //b ^= pow(2,bPos)
                b[bIndex] ^= 1 << bPos;   // Magic centaurian operation
            }
        }
    }
    return b;
}

template <class T, class L>
T bitAt(const T num, const L bit)
{
    return ((num >> bit) & 1);
}

template <class T>
bool isChecked(const T check, const T bit)
{
    return (bitAt(check, bit) == 1);
}

template <class T, class L>
T reverse(const T num, const L msb)
{
    auto rev = 0;
    for(auto i=0; i<msb; i++) {
        rev ^= (bitAt(num, i) << (msb-i-1));
    }
    return rev;
}

size_t numOnes(size_t n) {
    bitset<sizeof(size_t) * CHAR_BIT> b(n);
    return b.count();
}

template <class T, class L>
void printBinary(const T num, const L len)
{
    for(auto i=0; i<len;i++)
    {
        cerr << bitAt(num, i);
    }
    cerr << endl;
}

void inner(const u32 x, const u32 y, const u32 xCheck, const u32 yCheck, const u8 bPos, const int threadNum) {
    if(x < y) return; //we can add inverses of results at the end, don't check here
    iterations++;
    if(bPos < msb) {
        vector<pair<u8, u8>> possible = {make_pair(0,0), make_pair(0,1), make_pair(1,0), make_pair(1,1)};
        
        vector<pair<u8, u8>> valid = {};
        auto curPairs = pairs[bPos];
        auto unknownX = -1;
        auto unknownY = -1;
        //we can assume there are 2 unanswered values in the next row
        //find out the possible valid substitutions and try all of them
        for (auto it = possible.begin(); it != possible.end(); ++it)
        {
            auto xy = *it;
            auto test = 0;
            for (auto cpIt = curPairs.begin(); cpIt != curPairs.end(); ++cpIt)
            {
                auto curPair = *cpIt;
                auto xVal = bitAt(x, curPair.first);
                auto yVal = bitAt(y, curPair.second);
                if(!isChecked(xCheck, curPair.first)) {
                    unknownX = curPair.first;
                    xVal = xy.first;
                }
                if(!isChecked(yCheck, curPair.second)) {
                    unknownY = curPair.second;
                    yVal = xy.second;
                }
                test ^= (xVal & yVal);
            }
            if(test == bitAt(target0, bPos)) {
                valid.push_back(xy);
            }
        }
        auto newXCheck = xCheck ^ (1 << unknownX);
        auto newYCheck = yCheck ^ (1 << unknownY);
        auto index = 0;
        for (auto it = valid.begin(); it != valid.end(); ++it)
        {
            auto validPair = *it;
            auto newX = x ^ (validPair.first << unknownX);
            auto newY = y ^ (validPair.second << unknownY);
            auto revX = reverse(newX, msb);
            auto revY = reverse(newY, msb);
            auto okToPush = true;
            
            //remove cases where second answer doesn't sum to zero
            for(auto i=1; i<bPos; i++) {
                auto cutX = revX >> i;
                if(cutX == 0 || newY == 0) break;
                auto sum = cutX & newY;
                auto oneCnt = numOnes(sum);
                if(oneCnt % 2 == 1) {
                    okToPush = false;
                    break;
                }
            }
            
            //remove cases where there are exta 1s added in bits position size to 32
            if(okToPush && bPos == msb - 1) {
                auto b = Forward({revX, revY}, msb);
                okToPush = (b.size() > 0);
            }
            
            if(okToPush) {
                //keep spawning threads if we can
                if(bPos == SPAWN_INDEX && num_threads < MAX_THREADS && threadNum == 0) {
                    num_threads++;
                    candidates.push_back({});
                    threads.push_back(thread(inner, newX, newY, newXCheck, newYCheck, bPos+1, (int)num_threads));
                } else {
                    inner(newX, newY, newXCheck, newYCheck, bPos+1, threadNum);
                }
            }
            index++;
        }
        
    } else {
        auto newPair = make_pair(reverse(x, msb), reverse(y, msb));
        candidates[threadNum-1].push_back(newPair);
    }
}


vector<pair<u32, u32>> Backward() {
    
    chrono::high_resolution_clock::time_point before = chrono::high_resolution_clock::now();
    msb = 0; //most significant bit of target
    auto reduce = target0;
    while(reduce > 0) {
        reduce = reduce >> 1;
        msb++;
    }
    
    pairs.resize(msb);
    for (auto i = 0; i < msb; i++) {
        for (auto j = 0; j < msb; j++) {
            u32 zeroOrOne = (i + j) / msb;
            auto isZero = (zeroOrOne == 0);
            u8 bPos = (i + j) % msb;
            auto firstPos = i % msb;
            auto secondPos = j % msb;
            //need to inverse back into position
            auto firstPosInv = msb - firstPos - 1;
            auto secondPosInv = msb - secondPos - 1;
            if(isZero) {
                pairs[bPos].push_back(make_pair(firstPosInv, secondPosInv));
            }
        }
    }
    
    //recurse to find all possible pairs of numbers that work
    inner(0, 0, 0, 0, 0, 0);
    
    for (auto i = 0; i < num_threads; i++) {
        if(threads.size() > i) threads[i].join();
    }
    
    //merge candidates from all threads
    vector<pair<u32, u32>> total;
    for (auto it1 = candidates.begin(); it1 != candidates.end(); ++it1)
    {
        auto threadCandidates = *it1;
        for (auto it2 = threadCandidates.begin(); it2 != threadCandidates.end(); ++it2)
        {
            auto pair = *it2;
            total.push_back(make_pair(pair.first, pair.second));
        }
    }
    //include reflected values
    for (auto it1 = candidates.begin(); it1 != candidates.end(); ++it1)
    {
        auto threadCandidates = *it1;
        for (auto it2 = threadCandidates.begin(); it2 != threadCandidates.end(); ++it2)
        {
            auto pair = *it2;
            total.push_back(make_pair(pair.second, pair.first));
        }
    }
    //sort
    sort(total.begin(), total.end());
    
    chrono::high_resolution_clock::time_point after = chrono::high_resolution_clock::now();
    auto duration = chrono::duration_cast<chrono::milliseconds>( after - before ).count();
    
    cerr << "Time: " << duration << " milliseconds" << endl;
    cerr << "Threads: " << num_threads << endl;
    cerr << "Iterations: " << iterations << endl;
    cerr << "Solutions: " << total.size() << endl;
    
    return total;
}


int main()
{
    
#ifdef editor
    size = 32;
#else
    cin >> size;
#endif
    
    shrunkSize = size / 16;
    vector<u32> a(shrunkSize);
    
#ifdef editor
    //Test 1
    //a[0] = 29615; //111001110101111
    //a[1] = 0;
    //Test 2
    a[0] = 1937995713; //1110011100000110111011111000001
    a[1] = 0;
#else
    for (int i = 0; i < shrunkSize; i++) {   // Read size / 16 integers to a
        cin >> hex >> a[i];
    }
#endif
    
    cerr << "Size: " << size << endl;
    cerr << "A: " << endl;
    for (int i = 0; i < shrunkSize; i++) {
        cerr << a[i];
        if(i < shrunkSize - 1) cerr << ",";
    }
    cerr << endl;
    
    target0 = a[0];
    target1 = a[1];
    auto as = Backward();
    
    for(auto it=as.begin(); it<as.end(); it++)
    {
        auto a = *it;
        cout << setfill('0') << setw(8) << hex << a.first;
        cout << " ";
        cout << setfill('0') << setw(8) << hex << a.second;
        cout << endl;
#ifdef editor
        auto b = Forward({a.first, a.second}, -1);
        if(b[0] != target0) {
            cerr <<  "Backward didn't work: " << b[0] << "!=" << target0 << endl;
            return -1;
        }
        if(b[1] != target1) {
            cerr <<  "Bad Answer: " << a.first << " " << a.second << endl;
            printBinary(a.first, msb);
            printBinary(a.second, msb);
            cerr <<  "Backward didn't work: " << b[0] << "=" << target0 << ", " << b[1] << "!=" << target1 << endl;
            return -1;
        }
#endif
    };
    
    return 0;
}
